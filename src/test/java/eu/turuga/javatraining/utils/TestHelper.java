package eu.turuga.javatraining.utils;

import eu.turuga.javatraining.api.model.InputUserDto;
import eu.turuga.javatraining.api.model.ResourceCreatedDto;
import eu.turuga.javatraining.api.model.Status;
import eu.turuga.javatraining.api.model.User;
import eu.turuga.javatraining.domain.CountryDO;
import eu.turuga.javatraining.domain.UserDO;
import eu.turuga.javatraining.entity.CountryEntity;
import eu.turuga.javatraining.entity.UserEntity;

public class TestHelper {
    public final static String ID="test01";
    private static final String USER_COUNTRY = "Romania";
    public static final String USER_EMAIL = "test@test.com";
    private static final String USER_FIRSTNAME = "John";
    private static final String USER_LASTNAME = "Doe";
    private static final String USER_DRIVELICENCE = "XXX-ZZZ";
    private static final String USER_PASSWORD = "password";
    private static final String USER_COUNTRY_ISO = "ro";
    public static final String USER_UPDATED_FIRSTNAME = "Ion";

    public static ResourceCreatedDto getResourceCreatedDto() {
        ResourceCreatedDto resourceCreatedDto = new ResourceCreatedDto();
        resourceCreatedDto.setId(ID);
        return resourceCreatedDto;
    }

    public static User getUser() {
        User user=new User();
        user.setEmail(USER_EMAIL);
        user.setCountryOfResidence(USER_COUNTRY);
        user.setFirstName(USER_FIRSTNAME);
        user.setLastName(USER_LASTNAME);
        user.setDriverLicenceNumber(USER_DRIVELICENCE);
        user.setPassword(USER_PASSWORD);
        user.setStatus(Status.ACTIVE);
        return user;
    }

    public static UserDO getUserDO() {
        return UserDO
                .builder()
                .email(USER_EMAIL)
                .countryOfResidence(CountryDO
                        .builder()
                        .name(USER_COUNTRY)
                        .isoCode(USER_COUNTRY_ISO)
                        .build()
                )
                .driverLicenceNumber(USER_DRIVELICENCE)
                .lastName(USER_LASTNAME)
                .firstName(USER_FIRSTNAME)
                .status(Status.ACTIVE)
                .password(USER_PASSWORD)
                .id(ID)
                .build();
    }

    public static InputUserDto getInputUserDtoUpdate() {
        InputUserDto userDto= new InputUserDto();
        userDto.setFirstName(USER_UPDATED_FIRSTNAME);
        userDto.setLastName(USER_LASTNAME);
        userDto.setDriverLicenceNumber(USER_DRIVELICENCE);
        userDto.setEmail(USER_EMAIL);
        return userDto;
    }

    public static UserEntity getUserEntity() {

        CountryEntity countryEntity=new CountryEntity();
        countryEntity.setIsoCode(USER_COUNTRY_ISO);
        countryEntity.setName(USER_COUNTRY);

        UserEntity userEntity= new UserEntity();
        userEntity.setId(ID);
        userEntity.setEmail(USER_EMAIL);
        userEntity.setLastName(USER_LASTNAME);
        userEntity.setFirstName(USER_FIRSTNAME);
        userEntity.setDriverLicenceNumber(USER_DRIVELICENCE);
        userEntity.setEmail(USER_EMAIL);
        userEntity.setPassword(USER_PASSWORD);
        userEntity.setStatus(Status.ACTIVE);
        userEntity.setCountryOfResidence(countryEntity);
        return userEntity;
    }

    public static UserEntity getUserEntityEmpty() {
        return new UserEntity();
    }

    public static InputUserDto getInputUserDtoUpdateEmpty() {
        return new InputUserDto();
    }
}
