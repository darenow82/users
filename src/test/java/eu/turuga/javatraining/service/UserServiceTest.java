package eu.turuga.javatraining.service;

import eu.turuga.javatraining.mapper.UserMapperImpl;
import eu.turuga.javatraining.repository.UserRepository;
import eu.turuga.javatraining.utils.TestHelper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;

class UserServiceTest {
    @Mock
    private UserRepository userRepository;

    private UserService userService;


    @BeforeEach
    void init(){
        MockitoAnnotations.initMocks(this);
        this.userService=new UserService(userRepository,new UserMapperImpl());
    }

    @Test
    void createUserShouldCreateOk() {
        Mockito.when(userRepository.save(any())).thenReturn(TestHelper.getUserEntity());
        Assertions.assertEquals(TestHelper.ID,userService.createUser(TestHelper.getUserDO()).getId());
    }

    @Test
    void deleteUserByIdShouldDeleteIfUserFound() {
        Mockito.when(userRepository.existsById(TestHelper.ID)).thenReturn(true);
        Mockito.doNothing().when(userRepository).deleteById(TestHelper.ID);
        Assertions.assertDoesNotThrow(()->userService.deleteUserById(TestHelper.ID));
    }

    @Test
    void deleteUserByIdShouldNOTDeleteIfUserNOTFound() {
        Mockito.when(userRepository.existsById(TestHelper.ID)).thenReturn(false);
        Assertions.assertDoesNotThrow(()->userService.deleteUserById(TestHelper.ID));
    }

    @Test
    void getUserByIdShouldReturnIfUserFound() {
        Mockito.when(userRepository.findById(TestHelper.ID)).thenReturn(Optional.of(TestHelper.getUserEntity()));
        Assertions.assertEquals(TestHelper.ID,userService.getUserById(TestHelper.ID).getId());
    }

    @Test
    void getUsersShouldReturnAList() {
        Mockito.when(userRepository.findAll()).thenReturn(List.of(TestHelper.getUserEntity()));
        Assertions.assertEquals(1,userService.getUsers().size());
    }

    @Test
    void updateUserByIdShouldUpdateUserIfFound() {
        Mockito.when(userRepository.existsById(TestHelper.ID)).thenReturn(true);
        Mockito.when(userRepository.findById(TestHelper.ID)).thenReturn(Optional.of(TestHelper.getUserEntity()));
        Assertions.assertDoesNotThrow(()->userService.updateUserById(TestHelper.ID,TestHelper.getInputUserDtoUpdate()));
    }

    @Test
    void updateUserByIdShouldNotUpdateUserIfNotFound() {
        Mockito.when(userRepository.existsById(TestHelper.ID)).thenReturn(false);
        Assertions.assertDoesNotThrow(()->userService.updateUserById(TestHelper.ID,TestHelper.getInputUserDtoUpdate()));
    }

    @Test
    void updateUserByIdShouldNotUpdateUserForNullFields() {
        Mockito.when(userRepository.existsById(TestHelper.ID)).thenReturn(true);
        Mockito.when(userRepository.findById(TestHelper.ID)).thenReturn(Optional.of(TestHelper.getUserEntity()));
        Assertions.assertDoesNotThrow(()->userService.updateUserById(TestHelper.ID,TestHelper.getInputUserDtoUpdateEmpty()));
    }

}