package eu.turuga.javatraining.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.turuga.javatraining.TestConfig;
import eu.turuga.javatraining.api.model.UserDto;
import eu.turuga.javatraining.mapper.CountryMapper;
import eu.turuga.javatraining.mapper.UserMapper;
import eu.turuga.javatraining.service.UserService;
import eu.turuga.javatraining.utils.TestHelper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ContextConfiguration(classes = {TestConfig.class, CountryMapper.class, UserMapper.class} )
@WebMvcTest(UserController.class)
class UserControllerTest {

    @MockBean
    UserService userService;

    @Autowired
    UserMapper mapper;

    @Autowired
    MockMvc mockMvc;

    private static ObjectMapper objectMapper;

    @BeforeAll
    static void init() {
        objectMapper = new ObjectMapper();

    }

    @Test
    void createUserShouldCreateUser() throws Exception {
        Mockito.when(userService.createUser(any())).thenReturn(TestHelper.getUserDO());
        var result = mockMvc.perform(MockMvcRequestBuilders.post("/user")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(TestHelper.getUser())))
                .andExpect(status().isCreated())
                .andReturn();

        var userDto = objectMapper.readValue(result.getResponse().getContentAsString(), UserDto.class);
        Assertions.assertEquals(TestHelper.ID, userDto.getId());
    }

    @Test
    void deleteUserByIdReturnOkIfIdFound() throws Exception {
        Mockito.doNothing().when(userService).deleteUserById(any());
        var result = mockMvc.perform(MockMvcRequestBuilders.delete("/user/"+TestHelper.ID))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    void getUserById() throws Exception {
        Mockito.when(userService.getUserById(TestHelper.ID)).thenReturn(TestHelper.getUserDO());
        var result = mockMvc.perform(MockMvcRequestBuilders.get("/user/"+TestHelper.ID))
                .andExpect(status().isOk())
                .andReturn();

        var userDto = objectMapper.readValue(result.getResponse().getContentAsString(), UserDto.class);
        Assertions.assertEquals(TestHelper.USER_EMAIL, userDto.getEmail());

    }

    @Test
    void getUsers() throws Exception {
        Mockito.when(userService.getUsers()).thenReturn(List.of(TestHelper.getUserDO()));
        var result = mockMvc.perform(MockMvcRequestBuilders.get("/user"))
                .andExpect(status().isOk())
                .andReturn();

        var userDtoList = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<UserDto>>(){});
        Assertions.assertEquals(1, userDtoList.size());
        Assertions.assertEquals(TestHelper.USER_EMAIL,userDtoList.get(0).getEmail());
    }

    @Test
    void updateUserById() throws Exception {
        Mockito.doNothing().when(userService).updateUserById(TestHelper.ID,TestHelper.getInputUserDtoUpdate());
        var result = mockMvc.perform(MockMvcRequestBuilders.patch("/user/"+TestHelper.ID)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(TestHelper.getInputUserDtoUpdate()))
        )
                .andExpect(status().isOk())
                .andReturn();
    }
}