package eu.turuga.javatraining.controller;

import eu.turuga.javatraining.api.model.InputUserDto;
import eu.turuga.javatraining.api.model.UserDto;
import eu.turuga.javatraining.api.specification.UserApi;
import eu.turuga.javatraining.mapper.UserMapper;
import eu.turuga.javatraining.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
public class UserController implements UserApi {
    private final UserService userService;
    private final UserMapper mapper;

    public UserController(
            UserService userService,
            UserMapper mapper
    ){
        this.userService=userService;
        this.mapper=mapper;
    }

    @Override
    public ResponseEntity<UserDto> createUser(@Valid InputUserDto body) {
        return new ResponseEntity<>(
                mapper
                        .userDOtoUserDto(
                                userService.createUser(mapper.userToUserDO(body))
                        ),
                HttpStatus.CREATED
        );
    }



    @Override
    public ResponseEntity<Void> deleteUserById(String id) {
        userService.deleteUserById(id);
        return new ResponseEntity<>(HttpStatus.OK);

    }

    @Override
    public ResponseEntity<UserDto> getUserById(String id) {
        return new ResponseEntity<>(mapper.userDOtoUserDto(userService.getUserById(id)),HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<UserDto>> getUsers() {
        return new ResponseEntity<>(mapper.userDOListToUserDtoList(userService.getUsers()),HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> updateUserById(String id, @Valid InputUserDto body) {
        userService.updateUserById(id,body);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
