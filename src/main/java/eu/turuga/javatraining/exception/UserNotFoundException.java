package eu.turuga.javatraining.exception;

import org.springframework.http.HttpStatus;

public class UserNotFoundException extends CustomRestException{
    public UserNotFoundException() {
        super(
                CustomExceptionDto
                .builder()
                        .error_code(HttpStatus.NOT_FOUND.value())
                        .description("User not found!")
                        .error("user_not_found")
                .build()
        );
    }
}
