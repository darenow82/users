package eu.turuga.javatraining.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = { CustomRestException.class })
    protected ResponseEntity<CustomExceptionDto> handleException(CustomRestException ex, WebRequest request) {
        return new ResponseEntity<>(ex.getCustomExceptionDto(), HttpStatus.valueOf(ex.getCustomExceptionDto().getError_code()));
    }
}
