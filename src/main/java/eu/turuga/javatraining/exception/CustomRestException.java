package eu.turuga.javatraining.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class CustomRestException extends RuntimeException{
    private CustomExceptionDto customExceptionDto;
    public CustomRestException(CustomExceptionDto customExceptionDto){
        super();
        this.customExceptionDto = customExceptionDto;
    }
}
