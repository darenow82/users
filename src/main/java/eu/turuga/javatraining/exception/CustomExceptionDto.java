package eu.turuga.javatraining.exception;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomExceptionDto {
    private Integer error_code;
    private String error;
    private String description;
}
