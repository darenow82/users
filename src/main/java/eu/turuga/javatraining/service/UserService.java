package eu.turuga.javatraining.service;

import eu.turuga.javatraining.api.model.InputUserDto;
import eu.turuga.javatraining.domain.UserDO;
import eu.turuga.javatraining.entity.UserEntity;
import eu.turuga.javatraining.exception.UserNotFoundException;
import eu.turuga.javatraining.mapper.UserMapper;
import eu.turuga.javatraining.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    private final UserRepository userRepository;
    private final UserMapper mapper;

    public UserService(
            UserRepository userRepository,
            UserMapper mapper
    ) {
        this.userRepository = userRepository;
        this.mapper = mapper;
    }

    public UserDO createUser(UserDO userDO) {
        return mapper.userEntityToUserDO(userRepository.save(mapper.userDOToUserEntity(userDO)));
    }

    public void deleteUserById(String id) {
        if (userRepository.existsById(id)) {
            userRepository.deleteById(id);
        }
    }

    public UserDO getUserById(String id) {
        return mapper.userEntityToUserDO(userRepository.findById(id).orElseThrow(UserNotFoundException::new));
    }

    public List<UserDO> getUsers() {
        return mapper.userEntityListToUserDOList(userRepository.findAll());
    }

    public void updateUserById(String id, InputUserDto userDto) {
        if (userRepository.existsById(id)) {
            UserEntity userEntity=userRepository.findById(id).get();
            // need a better mapping here
            if (userDto.getFirstName()!=null) userEntity.setFirstName(userDto.getFirstName());
            if (userDto.getLastName()!=null) userEntity.setLastName(userDto.getLastName());
            if (userDto.getEmail()!=null) userEntity.setEmail(userDto.getEmail());
            if (userDto.getDriverLicenceNumber()!=null) userEntity.setDriverLicenceNumber(userDto.getDriverLicenceNumber());
            userRepository.save(userEntity);
        } else{
            throw new UserNotFoundException();
        }

    }
}
