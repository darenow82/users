package eu.turuga.javatraining.mapper;

import eu.turuga.javatraining.domain.CountryDO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CountryMapper {
    default CountryDO map(String value){
        return CountryDO.builder().name(value).build();
    }

    default String map(CountryDO value){
        if (value==null) {
            return null;
        }
        return value.getName();
    }

}
