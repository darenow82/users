package eu.turuga.javatraining.mapper;

import eu.turuga.javatraining.api.model.InputUserDto;
import eu.turuga.javatraining.api.model.UserDto;
import eu.turuga.javatraining.domain.UserDO;
import eu.turuga.javatraining.entity.UserEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring", uses = {CountryMapper.class})
public interface UserMapper {

    UserEntity userDOToUserEntity(UserDO userDO);

    UserDO userEntityToUserDO(UserEntity userEntity);

    List<UserDO> userEntityListToUserDOList(List<UserEntity> userEntityList);

    UserDO userToUserDO(InputUserDto user);


    UserDto userDOtoUserDto(UserDO userDO);

    List<UserDto> userDOListToUserDtoList(List<UserDO> userDOList);


}
