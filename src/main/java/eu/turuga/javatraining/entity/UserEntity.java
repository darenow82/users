package eu.turuga.javatraining.entity;


import eu.turuga.javatraining.api.model.Status;
import lombok.Data;
import org.springframework.data.annotation.Id;

@Data
public class UserEntity {
    @Id
    private String id;
    private String password;
    private String email;
    private String firstName;
    private String lastName;
    private CountryEntity countryOfResidence;
    private String driverLicenceNumber;
    private Status status;

}
