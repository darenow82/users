package eu.turuga.javatraining.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "country")
public class CountryEntity {
    @Id
    private String isoCode;
    private String name;
}
