package eu.turuga.javatraining.domain;

import eu.turuga.javatraining.api.model.Status;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserDO {
    private String id;
    private String password;
    private String email;
    private String firstName;
    private String lastName;
    private CountryDO countryOfResidence;
    private String driverLicenceNumber;
    private Status status;

}
